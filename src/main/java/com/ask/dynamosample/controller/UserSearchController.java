package com.ask.dynamosample.controller;

import com.ask.dynamosample.entities.UserDetail;
import com.ask.dynamosample.service.UserSearchService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserSearchController {

    private UserSearchService userSearchService;

    UserSearchController(UserSearchService userSearchService) {
        this.userSearchService = userSearchService;
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserDetail> searchUserById(@PathVariable("userId") String userId) {
        UserDetail userDetail = userSearchService.findByUserId(userId);
        if (userDetail == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(userDetail);
    }

    @PostMapping
    public ResponseEntity<UserDetail> addNewUser(@RequestBody UserDetail userDetail) {
        UserDetail userDetailPersisted = userSearchService.addNewUser(userDetail);
        return ResponseEntity.created(URI.create("/users/" + userDetailPersisted.getId())).body(userDetailPersisted);
    }

    @GetMapping("/")
    public ResponseEntity<List<UserDetail>> getAllUserDetails() {
        List<UserDetail> userDetails = userSearchService.findAllUser();
        if (userDetails == null ||  userDetails.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(userDetails);
    }
}
