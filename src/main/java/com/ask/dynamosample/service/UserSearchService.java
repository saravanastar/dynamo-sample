package com.ask.dynamosample.service;

import com.ask.dynamosample.entities.UserDetail;
import com.ask.dynamosample.repositories.UserDetailRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserSearchService {
    private UserDetailRepository userDetailRepository;

    public UserSearchService(UserDetailRepository userDetailRepository) {
        this.userDetailRepository = userDetailRepository;
    }

    public List<UserDetail> findAllUser() {
        return StreamSupport.stream(userDetailRepository.findAll().spliterator(), true).collect(Collectors.toList());
    }

    public UserDetail addNewUser(UserDetail userDetail) {
        return userDetailRepository.save(userDetail);
    }

    public UserDetail findByUserId(String userId) {
        return userDetailRepository.findById(userId).orElse(null);
    }
}
