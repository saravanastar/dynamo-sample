package com.ask.dynamosample;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.local.embedded.DynamoDBEmbedded;
import com.amazonaws.services.dynamodbv2.local.main.ServerRunner;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;
import com.amazonaws.services.dynamodbv2.local.shared.access.AmazonDynamoDBLocal;
import com.ask.dynamosample.service.UserSearchService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.event.annotation.AfterTestClass;
import org.springframework.test.context.event.annotation.BeforeTestClass;

@SpringBootTest
@ActiveProfiles("unittest")
class DynamoSampleApplicationTests {
	private static DynamoDBProxyServer server;

	@Autowired
	UserSearchService userSearchService;

//	@BeforeTestClass
//	public static void setupClass() throws Exception {
//		System.setProperty("sqlite4java.library.path", "native-libs");
//		String port = "8000";
//		server = ServerRunner.createServerFromCommandLineArgs(
//				new String[]{"-inMemory", "-port", port});
//		server.start();
//	}
//
//	@AfterTestClass
//	public static void teardownClass() throws Exception {
//		server.stop();
//	}

	@Test
	void contextLoads() {

		Assert.assertNotNull(userSearchService);
	}
}
