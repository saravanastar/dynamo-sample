package com.ask.dynamosample.service;

import com.ask.dynamosample.entities.UserDetail;
import com.ask.dynamosample.repositories.UserDetailRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserSearchServiceTest {

    @Mock
    UserDetailRepository userDetailRepository;

    @InjectMocks
    UserSearchService userSearchService;

    @Test
    public void testFindAllUserWithEmptyIterable() {
        when(userDetailRepository.findAll()).thenReturn(Arrays.asList());
        List<UserDetail> response  = userSearchService.findAllUser();
        Assert.assertNotNull(response);
    }

    @Test
    public void testFindAllUserWithSingleUserDetails() {
        UserDetail userDetail = UserDetail.builder().emailAddress("test@test.com").name("ddd").id("111").build();

        when(userDetailRepository.findAll()).thenReturn(Arrays.asList(userDetail));
        List<UserDetail> response  = userSearchService.findAllUser();
        Assert.assertNotNull(response);
        Assert.assertEquals(1, response.size());
    }
}
